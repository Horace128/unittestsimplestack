import java.util.*;

public class SimpleStack<E> {

	/*
	 * public SimpleStack() { // TODO Auto-generated constructor stub
	 * 
	 * }
	 */
	private ArrayList<E> data;

	public SimpleStack() {
		data = new ArrayList<E>();
	}

	public int clear() {
		data.clear();
		return data.size();
		
	}
	
	// contains - searchs the stack for the item requested by the user
	public int contains(Object o) {
		if (data.size() == 0)
			return -1;
		else if (data.contains(o)) {
			return data.indexOf(o) + 1;
		} else
			return -1;

	}

	// peek - looks at the last thing in the stack without removing it
	public E peek() {
		if (data.isEmpty())
			throw new EmptyStackException();
		else
			return data.get(data.size()-1);
	}

	// push - inserts the obj into the stack
	public E push(E obj) {
		data.add(obj);
		return obj;
	}

	// pop - pops the last item off the stack
	public E pop() {
		if (data.size() == 0) {
			throw new EmptyStackException();
		} else {
			return data.remove(data.size()-1); // the ArrayList method will remove and return it,
		}
	}

	// empty -tests if the stack is empty
	public Boolean isEmpty() {
		if (data.size() == 0)
			return true;
		else
			return false;
	}

	// toString - toString for the stack
	public String toString() {
		return data.toString();
	}
}
