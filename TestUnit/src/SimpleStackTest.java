import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test; 

class SimpleStackTest {

	@Test
	void test() {
		SimpleStack<Integer> test = new SimpleStack<Integer>();
		
		//Test push method
		int actual = test.push(5);
		assertEquals(5, actual);
		
		//Test contains method
		int actual2 = test.contains(5);
		assertEquals(1, actual2);
		int actual3 = test.contains(2);
		assertEquals(-1, actual3);
		
		//import more values for stack
		test.push(6);
		test.push(9);
		test.push(23);
		test.push(1);
		
		//test peek method
		int actual4 = test.peek();
		assertEquals(1, actual4);
		
		//test pop method
		int actual5 = test.pop();
		assertEquals(1, actual5);
		
		//test isEmpty method
		boolean actual6 = test.isEmpty();
		assertEquals(false, actual6);
		
		//test clear method
		int actual7 = test.clear();
		assertEquals(0, actual7);
		
		//test isEmpty method again
		boolean actual8 = test.isEmpty();
		assertEquals(true, actual8);
		
	}

}
